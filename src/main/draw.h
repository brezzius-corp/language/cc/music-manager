#ifndef __DRAW_H
#define __DRAW_H

void setPixel(int, int, PIXEL, PIXEL *image32, int XRES);
void clearWindow(PIXEL *image32, int XRES, int width, int height);
void update_image(Display *display, Window window, GC gc, XImage *ximage, PIXEL *image32, FMOD_CHANNEL *canal, int XRES, int width, int height);

#endif /* __DRAW_H */
