#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <sys/select.h>

#include "timer.h"

long int getTicks(void)
{
    long int ms; /* Milliseconds */
    time_t s;  /* Seconds */
    struct timeval tv;

    gettimeofday(&tv, NULL);

    s  = tv.tv_sec;
    ms = floor(tv.tv_usec / 1.0e3);

    return s*1000+ms;
}

void getLength(unsigned int length)
{
    unsigned int len = length, ms, sec, min;

    ms = len%1000;
    sec = len/1000;

    if(sec >= 60)
    {
        min = sec/60;
        sec = sec%60;
        printf("\nLongueur du morceau : %d:%d.%d\n", min, sec, ms);
    }
    else
        printf("\nLongueur du morceau : %d.%d\n", sec, ms);
    
}

void printTime(unsigned int time, unsigned int length)
{
    unsigned int len = time, ms = 0, sec = 0, min = 0;
    unsigned int blen = length, bms = 0, bsec = 0, bmin = 0;

    ms = len%1000;
    sec = len/1000;

    if(sec >= 60)
    {
        min = sec/60;
        sec = sec%60;
    }

    bms = blen%1000;
    bsec = blen/1000;

    if(bsec >= 60)
    {
        bmin = bsec/60;
        bsec = bsec%60;
    }

    printf("Lecture : %d:%.2d.%.3d / %d:%.2d.%.3d\r", min, sec, ms, bmin, bsec, bms);
}

char *getTime(unsigned int time, unsigned int length)
{
    unsigned int len = time, ms = 0, sec = 0, min = 0;
    unsigned int blen = length, bms = 0, bsec = 0, bmin = 0;

    char *text;
    
    if(!(text = malloc(180 * sizeof(char))))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    ms = len%1000;
    sec = len/1000;

    if(sec >= 60)
    {
        min = sec/60;
        sec = sec%60;
    }

    bms = blen%1000;
    bsec = blen/1000;

    if(bsec >= 60)
    {
        bmin = bsec/60;
        bsec = bsec%60;
    }

    sprintf(text, "%d:%.2d.%.3d / %d:%.2d.%.3d", min, sec, ms, bmin, bsec, bms);

    return text;
}

void msleep(int tms)
{
    struct timeval tv;
    tv.tv_sec  = tms / 1000;
    tv.tv_usec = (tms % 1000) * 1000;
    select(0, NULL, NULL, NULL, &tv);
}

