#include <stdio.h>
#include <stdlib.h>
#include <X11/Xlib.h>
#include <fmodex/fmod.h>

#include "constants.h"
#include "draw.h"

void setPixel(int x, int y, PIXEL clr, PIXEL *image32, int XRES)
{
    *(image32 + x + y * XRES) = clr;
}

void clearWindow(PIXEL *image32, int XRES, int width, int height)
{
    PIXEL *pos;

    int i, j;

    pos = image32;

    for(j = 0 ; j < height ; j++)
    {
        for(i = 0 ; i < width ; i++)
            *(pos++) = 0x000000;

        pos += XRES - width;
    }
}

void update_image(Display *display, Window window, GC gc, XImage *ximage, PIXEL *image32, FMOD_CHANNEL *canal, int XRES, int width, int height)
{
    int hauteurBarre;
    int i, j;
    int r, g, b = 0;
    float spectre[TAILLE_SPECTRE];
  
    clearWindow(image32, XRES, width, height);

    FMOD_Channel_GetSpectrum(canal, spectre, TAILLE_SPECTRE, 0,  FMOD_DSP_FFT_WINDOW_RECT);

    for(i = 0 ; i < TAILLE_SPECTRE ; i++)
    {
        hauteurBarre = spectre[i] * 20 * height;

        if (hauteurBarre > height)
            hauteurBarre = height;

        for(j = height - hauteurBarre ; j < height ; j++)
        {
               r = 255 - (j/(height/255.));
               g = j/(height/255.);

               setPixel(i, j, (r<<16)|(g<<8)|b, image32, XRES);
        }
    }

    XPutImage(display, window, gc, ximage, 0, 0, 0, 0, width, height);  
}

