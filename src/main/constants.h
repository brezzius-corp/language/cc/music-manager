#ifndef __CONSTANTS_H
#define __CONSTANTS_H

typedef unsigned int PIXEL;

#define WIDTH_WINDOW 1024 
#define HEIGHT_WINDOW 800 /* Vous pouvez la faire varier celle-là par contre */
#define RATIO (HEIGHT_WINDOW / 255.0)
#define PERIOD 25 /* Temps en ms entre chaque mise à jour du graphe. 25 ms est la valeur minimale. */
#define TAILLE_SPECTRE 1024

#endif /* __CONSTANTS_H */
