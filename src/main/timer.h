#ifndef __TIMER_H
#define __TIMER_H

long int getTicks(void);
void getLength(unsigned int length);
void printTime(unsigned int time, unsigned int length);
char *getTime(unsigned int time, unsigned int length);
void msleep(int tms);

#endif /* __TIMER_H */

