#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "function.h"

char *setup(int argc, char *argv[], int *loop)
{
    char *fileName;

    switch(argc)
    {
        case 2:
            if(!(fileName = malloc(strlen(argv[1]) * sizeof(char) + 1)))
            {
                fprintf(stderr, "Memory error\n");
                exit(EXIT_FAILURE);
            }
            strcpy(fileName, argv[1]);
            break;
        case 3:
            if(!(fileName = malloc(strlen(argv[2]) * sizeof(char) + 1)))
            {
                fprintf(stderr, "Memory error\n");
                exit(EXIT_FAILURE);
            }
            strcpy(fileName, argv[2]);
            *loop = 100;
            break;
        default:
            fprintf(stderr, "Bad format\n");
            exit(EXIT_FAILURE);
            break;
    }

    return fileName;
}

void handler()
{
    printf("\r\n\rInterruption du programme\n");
    exit(1);
}

