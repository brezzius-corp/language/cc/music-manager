#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <X11/Xlib.h>
#include <fmodex/fmod.h>

#include "constants.h"
#include "function.h"
#include "timer.h"
#include "draw.h"

int main(int argc, char *argv[])
{
    int running = 1, pause = 0, kc, screen, loop = 1, tempsActuel, tempsPrecedent = 0, h, w, i;
    int coordix;
    int XRES, YRES, width, height;
    int keysyms_per_keycode_return;
    unsigned int length, current = 0;
    float volume;
    char *keycode;
    char *tagname[] = {"ARTIST", "TITLE", "ALBUM", "YEAR", "COMMENT", "TRACK", "GENRE"};

    /* Variable X11 */
    Display *display;
    Window window;
    XImage *ximage;
    XFontStruct *font;
    Visual *visual;
    KeySym *keysym;
    Atom wmDelete;
    PIXEL *image32;
    XEvent event;
    GC gc;

    /* Variable FMOD */
    FMOD_SYSTEM *system;
    FMOD_SOUND *musique;
    FMOD_RESULT resultat;
    FMOD_CHANNEL *canal;
    FMOD_TAG tag;

    /* Vérifications des arguments ligne de commandes */
    char *fileName = setup(argc, argv, &loop);

    /* Ouverture d'une connexion grahique avec le serveur */
    if(!(display = XOpenDisplay(getenv("DISPLAY"))))
    {
        fprintf(stderr, "Unable to load display\n");
        exit(EXIT_FAILURE);
    }

    screen = DefaultScreen(display);

    visual = DefaultVisual(display, 0);
    if(visual->class!=TrueColor)
    {
        fprintf(stderr, "Cannot handle non true color visual\n");
        XCloseDisplay(display);
        exit(EXIT_FAILURE);
    }

    XRES = DisplayWidth(display,screen);
    YRES = DisplayHeight(display,screen);

    width = XRES/2;
    height = YRES/2;

    if(!(image32 = malloc(XRES*YRES*4)))
    {
        fprintf(stderr, "Memory error\n");
        exit(EXIT_FAILURE);
    }

    ximage = XCreateImage(display, visual, 24, ZPixmap, 0, (char *)image32, XRES, YRES, 32, 0);

    window = XCreateSimpleWindow(display, RootWindow(display, 0), 0, 0, XRES, YRES, 1, 0, 0);

    if(!(gc = XCreateGC(display, window, 0, NULL)))
    {
        fprintf(stderr, "Unable to create gc\n");
        exit(EXIT_FAILURE);
    }

    /* Load fonts */
    if(!(font = XLoadQueryFont(display, "Linux Libertine")))
    {
        fprintf(stderr, "Unable to open font\n\n");
        if(!(font = XLoadQueryFont(display, "fixed")))
            exit(EXIT_FAILURE);
    }

    /* Intercept windowdow delete event */
    wmDelete=XInternAtom(display, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(display, window, &wmDelete, 1);

    /* Evenement */
    XSelectInput(display, window, KeyPressMask | ButtonPressMask | ExposureMask | StructureNotifyMask);

    /* Affichage de la fenêtre */
    XMapWindow(display, window);

    /* Initialisation de FMOD */
    FMOD_System_Create(&system);
    FMOD_System_Init(system, 1, FMOD_INIT_NORMAL, NULL);

    for(i = 0 ; i < loop ; i++)
    {
        /* Mise à jour de la boucle */
        running = 1;

        /* On ouvre la musique */
        if((resultat = FMOD_System_CreateSound(system, fileName, FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &musique)) != FMOD_OK)
        {
            fprintf(stderr, "Impossible de lire le fichier %s\n", fileName);
            exit(EXIT_FAILURE);
        }

        /* Info sur le morceau */
        for(i = 0 ; i < 7 ; i++)
        {
            if(FMOD_Sound_GetTag(musique, tagname[i], 0, &tag))
                printf("%s : None\n", tagname[i]);
            else
                printf("%s : %s\n", tagname[i], (char*)tag.data);
        }

        /* Longueur du morceau */
        FMOD_Sound_GetLength(musique, &length, FMOD_TIMEUNIT_MS);
        getLength(length);

        /* On joue la musique */
        FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, musique, 0, NULL);
    
        /* On récupère le pointeur du canal */
        FMOD_System_GetChannel(system, 0, &canal);

        /* Paramètre d'écriture */
        XSetForeground(display, gc, WhitePixel(display, screen));
        XSetFont(display, gc, font->fid);

        signal(SIGINT, handler);

        while(running)
        {
            while(XPending(display))
            {
                XNextEvent(display, &event);
                switch(event.type)
                {
                    case Expose:
                        if(event.xexpose.count == 0)
                            update_image(display, window, gc, ximage, image32, canal, XRES, width, height);
                        break;
                    case ButtonPress:
                        switch(event.xbutton.button)
                        {
                            case Button1:
                                coordix = event.xbutton.x;
                                printf("\n%d\n", coordix);
                                break;
                        }
                        break;
                    case MotionNotify:
                            printf("cas\n");
                            printf("%d\n", event.xmotion.x);
                            printf("%d\n", event.xmotion.y);
                        break;
                    case KeyPress:
                        kc = ((XKeyPressedEvent*)&event)->keycode;

                        keysym = XGetKeyboardMapping(display, kc, 1, &keysyms_per_keycode_return);
                        keycode = XKeysymToString(keysym[0]);

                        XFree(keysym);

                        if(!strcmp(keycode, "p") || !strcmp(keycode, "space"))
                        {
                            if(pause)
                            {
                                FMOD_Channel_SetPaused(canal, 0);
                                pause = 0;
                            }
                            else
                            {
                                FMOD_Channel_SetPaused(canal, 1);
                                pause = 1;
                            }
                        }
                        else if(!strcmp(keycode, "Left"))
                        {
                            if((int)current - 5000 > 0)
                                FMOD_Channel_SetPosition(canal, current - 5000, FMOD_TIMEUNIT_MS);
                            else
                                FMOD_Channel_SetPosition(canal, 0, FMOD_TIMEUNIT_MS);
                        }
                        else if(!strcmp(keycode, "Right"))
                        {
                            if((int)current + 5000 < (int)length)
                                FMOD_Channel_SetPosition(canal, current + 5000, FMOD_TIMEUNIT_MS);
                            else
                                running = 0;
                        }
                        else if(!strcmp(keycode, "Up"))
                        {
                            if((int)current + 60000 < (int)length)
                                FMOD_Channel_SetPosition(canal, current + 60000, FMOD_TIMEUNIT_MS);
                            else
                                running = 0;
                        }
                        else if(!strcmp(keycode, "Down"))
                        {
                            if((int)current - 60000 > 0)
                                FMOD_Channel_SetPosition(canal, current - 60000, FMOD_TIMEUNIT_MS);
                            else
                                FMOD_Channel_SetPosition(canal, 0, FMOD_TIMEUNIT_MS);
                        }
                        else if(!strcmp(keycode, "Next"))
                        {
                            if((int)current - 300000 > 0)
                                FMOD_Channel_SetPosition(canal, current - 300000, FMOD_TIMEUNIT_MS);
                            else
                                FMOD_Channel_SetPosition(canal, 0, FMOD_TIMEUNIT_MS);
                        }
                        else if(!strcmp(keycode, "Prior"))
                        {
                            if((int)current + 300000 > 0)
                                FMOD_Channel_SetPosition(canal, current + 300000, FMOD_TIMEUNIT_MS);
                            else
                                FMOD_Channel_SetPosition(canal, 0, FMOD_TIMEUNIT_MS);
                        }
                        else if(!strcmp(keycode, "asterisk"))
                        {
                            FMOD_Channel_GetVolume(canal, &volume);
                            FMOD_Channel_SetVolume(canal, volume+=0.1);
                        }
                        else if(!(strcmp(keycode, "colon")))
                        {
                            FMOD_Channel_GetVolume(canal, &volume);
                            FMOD_Channel_SetVolume(canal, volume-=0.1);
                        }
                        else if(!strcmp(keycode, "q"))
                        {
                            running = 0;
                            loop = 0;
                        }
                        break;
                    case ClientMessage:
                        if(event.xclient.data.l[0] == (int)wmDelete)
                        {
                            running = 0;
                            loop = 0;
                        }
                        break;
                    case ConfigureNotify:
                        /* Update width & height in case of windowdow resize */
                        w = event.xconfigure.width;
                        h = event.xconfigure.height;
                        if(w > 1)
                            width = w;
                        if(h > 1)
                            height = h;
                        break;
                    default :
                        break;
                }
            }

            /* Position du morceau */
            FMOD_Channel_GetPosition(canal, &current, FMOD_TIMEUNIT_MS);
            fflush(stdout);
            printTime(current, length);

            /* Morceau terminé */
            if(current == length)
                running = 0;

            /* Gestion du temps */
            tempsActuel = getTicks()%1000000;

            if(tempsActuel - tempsPrecedent < PERIOD)
                msleep((PERIOD - (tempsActuel - tempsPrecedent)));

            tempsPrecedent = getTicks()%1000000;

            update_image(display, window, gc, ximage, image32, canal, XRES, width, height);
            XDrawString(display, window, gc, 40, 50, getTime(current, length), strlen(getTime(current, length)));

        }

        FMOD_Sound_Release(musique);
    }

    /* LIbération de FMOD */
    FMOD_System_Release(system);

    /* Libération de l'image */
    free(image32);

    /* Libération de X11 */
    XFreeFont(display, font);
    XDestroyWindow(display, window);
    XCloseDisplay(display);

    return 0;
}

