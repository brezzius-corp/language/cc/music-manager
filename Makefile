# Makefile

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi
LDFLAGS=-lX11 -lm -lfmodex
EXECUTABLE=smx
EXECUTABLE_TEST=smx_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: smx
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/function.o $(SRC_BIN)/timer.o $(SRC_BIN)/draw.o $(LDFLAGS)

test: package smx_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

smx: init main.o function.o timer.o draw.o
	$(CC) -o $(SRC_BIN)/smx $(SRC_BIN)/main.o $(SRC_BIN)/function.o $(SRC_BIN)/timer.o $(SRC_BIN)/draw.o $(LDFLAGS)

smx_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/constants.h $(SRC_MAIN)/function.h $(SRC_MAIN)/timer.h $(SRC_MAIN)/draw.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

function.o: $(SRC_MAIN)/function.c $(SRC_MAIN)/function.h
	$(CC) -o $(SRC_BIN)/function.o -c $(SRC_MAIN)/function.c $(CFLAGS)

timer.o: $(SRC_MAIN)/timer.c $(SRC_MAIN)/timer.h
	$(CC) -o $(SRC_BIN)/timer.o -c $(SRC_MAIN)/timer.c $(CFLAGS)

draw.o: $(SRC_MAIN)/draw.c $(SRC_MAIN)/draw.h $(SRC_MAIN)/constants.h
	$(CC) -o $(SRC_BIN)/draw.o -c $(SRC_MAIN)/draw.c $(CFLAGS)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)

